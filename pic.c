#define PIC1 0x20 // IO base address for master PIC
#define PIC2 0xA0 // IO base address for slave PIC
#define PIC1_CMD PIC1
#define PIC1_DATA (PIC1+1)
#define PIC2_CMD PIC2
#define PIC2_DATA (PIC2+1)
#define PIC1_OFFSET 0x08
#define PIC2_OFFSET 0x70

#define PIC_EOI 0x20 // End-of-interrupt command code

#define IVT_BASE 0x0000

#define ICW4_8086 0x01 /* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO 0x02 /* Auto (normal) EOI */
#define ICW4_BUF_SLAVE 0x08 /* Buffered mode/slave */
#define ICW4_BUF_MASTER 0x0C /* Buffered mode/master */
#define ICW4_SFNM 0x10 /* Special fully nested (not) */
 
#include "io.h"
#include "types.h"
#include "isr.h"
#include "terminal.h"

/*
  256 interrupt vectors (0x00-0xFF)
  - Exceptions, first 32 vectors (0x00-0x1F)
  - Hardware interrupts
  -- 16 IRQ signals
  --- IRQs are translated to vectors by the PIC.
  --- Default 0x08-0x0F for IRQ #0-7
  --- Default 0x70-0x78 for IRQ #8-15
  -- Message-based interrupts
  - Software interrupts, typically vector 0x80.
*/

/*
  IRQ Assignments
  #0: Programmable Interrupt Timer
  #1: Keyboard
  #2: Slave PIC
  #3: COM2
  #4: COM1
  #5: LPT2
  #6: Floppy
  #7: LPT1/Spurious
  #8: CMOS RTC
  #9: Free/SCSI/NIC
  #10: Free/SCSI/NIC
  #11: Free/SCSI/NIC
  #12: PS/2 Mouse
  #13: FPU/Coprocessor/Interprocessor
  #14: ATA1
  #15: ATA2
*/

void pic_init(){

char a1,a2;

a1 = inb(PIC1_DATA);
a2 = inb(PIC2_DATA);

outb(PIC1_CMD, 0x11); // init command.
io_wait();
outb(PIC1_CMD, 0x11);
io_wait();

outb(PIC1_DATA, PIC1_OFFSET); // ICW2: Master PIC vector offset
io_wait();
outb(PIC2_DATA, PIC2_OFFSET); // ICW2: Slave PIC vector offset
io_wait();

outb(PIC1_DATA, 4); // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
io_wait();
outb(PIC2_DATA, 2); // ICW3: tell Slave PIC its cascade identity (0000 0010)
io_wait();

outb(PIC1_DATA, ICW4_8086);
io_wait();
outb(PIC2_DATA, ICW4_8086);
io_wait();
 
outb(PIC1_DATA, a1);   // restore saved masks.
outb(PIC2_DATA, a2);
}

u32 *ivt;

extern void isr_wrapper(void);

void ivt_init(){
ivt = (u32*)IVT_BASE;

u32 ptr = &isr_wrapper;
u32 segment = ptr/16;
u32 offset = ptr - (segment*16);
u32 reladd = (segment << 16) | offset;
ivt[9] = reladd; // keyboard
ivt[74] = reladd; // mouse

terminal_printf("Segment: %x\n", segment);
terminal_printf("Offset: %x\n", offset);
terminal_printf("IVT Base: %x\n", &ivt[0]);
terminal_printf("IVT Vector 9: %x\n", &ivt[9]);
terminal_printf("ISR Linear Address: %x\n", &isr_wrapper);
terminal_printf("ISR Relative Address: %x\n", reladd);

}

void pic_send_eoi(unsigned char irq)
{
if(irq >= 8)
  outb(PIC2_CMD,PIC_EOI);
 
outb(PIC1_CMD,PIC_EOI);
}
