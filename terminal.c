#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include "types.h"
#include "io.h"
#include "strings.h"

/* Hardware text mode color constants. */
enum vga_color
  {
    COLOR_BLACK = 0,
    COLOR_BLUE = 1,
    COLOR_GREEN = 2,
    COLOR_CYAN = 3,
    COLOR_RED = 4,
    COLOR_MAGENTA = 5,
    COLOR_BROWN = 6,
    COLOR_LIGHT_GREY = 7,
    COLOR_DARK_GREY = 8,
    COLOR_LIGHT_BLUE = 9,
    COLOR_LIGHT_GREEN = 10,
    COLOR_LIGHT_CYAN = 11,
    COLOR_LIGHT_RED = 12,
    COLOR_LIGHT_MAGENTA = 13,
    COLOR_LIGHT_BROWN = 14,
    COLOR_WHITE = 15,
  };
 
uint8_t make_color(enum vga_color fg, enum vga_color bg)
{
  return fg | bg << 4;
}
 
uint16_t make_vgaentry(char c, uint8_t color)
{
  uint16_t c16 = c;
  uint16_t color16 = color;
  return c16 | color16 << 8;
}
 
static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
 
size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;
 
void terminal_initialize()
{
  terminal_row = 0;
  terminal_column = 0;
  terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
  terminal_buffer = (uint16_t*) 0xB8000;
  for ( size_t y = 0; y < VGA_HEIGHT; y++ )
    {
      for ( size_t x = 0; x < VGA_WIDTH; x++ )
	{
	  const size_t index = y * VGA_WIDTH + x;
	  terminal_buffer[index] = make_vgaentry(' ', terminal_color);
	}
    }
}
 
void terminal_setcolor(uint8_t color)
{
  terminal_color = color;
}
 
void terminal_putentryat(char c, uint8_t color, size_t x, size_t y)
{
  const size_t index = y * VGA_WIDTH + x;
  terminal_buffer[index] = make_vgaentry(c, color);
}
 
void terminal_putchar(char c)
{
  terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
  if ( ++terminal_column == VGA_WIDTH )
    {
      terminal_column = 0;
    }
}

/* void update_cursor(int row, int col)
 * by Dark Fiber
 */
void update_cursor(size_t row, size_t col)
{

  if ( col >= VGA_WIDTH )
    {
      col = 0;
      if ( ++row >= VGA_HEIGHT )
	{
	  row = VGA_HEIGHT-1;
	}
    }

  unsigned short position=(row*80) + col;
 
  // cursor LOW port to vga INDEX register
  outb(0x3D4, 0x0F);
  outb(0x3D5, (unsigned char)(position&0xFF));
  // cursor HIGH port to vga INDEX register
  outb(0x3D4, 0x0E);
  outb(0x3D5, (unsigned char)((position>>8)&0xFF));
}

void terminal_shiftup(){
  for ( size_t y = 0; y < VGA_HEIGHT-1; y++ )
    {
      for ( size_t x = 0; x < VGA_WIDTH; x++ )
	{
	  const size_t index = y * VGA_WIDTH + x;
	  terminal_buffer[index] = terminal_buffer[index+VGA_WIDTH];
	}
    }

  for (size_t x = 0; x < VGA_WIDTH; x++){
    size_t y = VGA_HEIGHT-1;
    const size_t index = y * VGA_WIDTH + x;
    terminal_buffer[index] = make_vgaentry(' ', terminal_color);
  }
}

void terminal_newline(){
  terminal_column=0;
  terminal_row++;
  if (terminal_row == VGA_HEIGHT){
    terminal_shiftup();
    terminal_row = VGA_HEIGHT-1;
  }
}
 
void terminal_print(const char* data)
{
  size_t datalen = cstr_len(data);
  for ( size_t i = 0; i < datalen; i++ )
    if ( data[i] == '\n' )
      {
	terminal_newline();
      }else{
      terminal_putchar(data[i]);
    }
  update_cursor(terminal_row, terminal_column);
}

void terminal_printf(const char* format, ...){

  char dest[255];
  va_list args;
  va_start(args, format);
  cstr_vsprintf(dest, format, args);
  va_end(args);
  terminal_print(dest);
}

/* pending heap.
   void terminal_prints(string s)
   {
   for (u32 i = 0; i < s.length; i++ )
   if (s.data[i] == '\n' )
   {
   terminal_column = 0;
   terminal_row++;
   }else{
   terminal_putchar(s.data[i]);
   }
   update_cursor(terminal_row, terminal_column);
   }
*/
