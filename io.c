#include <stdint.h>


// github.com/primis/Apollo
void outb(unsigned short port, unsigned char data)
{
  __asm__ volatile ( "out dx, al" 
		     : // Output
		     : "d" (port), "a" (data) // Input
		     );
}

void outs(unsigned short port, unsigned short data)
{
  __asm__ volatile ( "out dx, ax" 
		     : // Output
		     : "d" (port), "a" (data) // Input
		     );
}

// http://f.osdev.org/viewtopic.php?f=1&t=12886
// http://x86.renejeschke.de/html/file_module_x86_id_222.html
void outl(unsigned short port, unsigned long data)
{
  __asm__ volatile("out dx, eax" 
		   : // Output
		   : "d" (port), "a" (data) // Input
		   );
}

// github.com/primis/Apollo
unsigned char inb(unsigned short port)
{
  unsigned char data;
  __asm__ volatile ( "in al, dx"
		     : "=a" (data) // Output
		     : "d" (port) // Input
		     );
  return data;
}

// github.com/primis/Apollo
unsigned short ins(unsigned short port)
{
  unsigned short data;
  __asm__ volatile ( "in ax, dx" 
		     : "=a"(data) // Output
		     : "d"(port) // Input
		     );
  return data;
}

// github.com/primis/Apollo
unsigned long inl(unsigned short port)
{
  unsigned long data;
  __asm__ volatile ( "in eax, dx"
		     : "=a" (data) // Output
		     : "d" (port) // Input
		     );
  return data;
}


void io_wait()
  {
  __asm__ volatile ( "jmp 1f\n\t" "1:jmp 2f\n\t" "2:" );
  }
