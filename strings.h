#include <stdarg.h>

u32 cstr_len(const char* str);
void cstr_sprintf(char* dest, const char* format, ...);
void cstr_vsprintf(char* dest, const char* format, va_list args);
