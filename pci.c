#include <stddef.h>
#include <stdint.h>
#include "types.h"
#include "strings.h"
#include "terminal.h"
#include "io.h"


#define CONFIG_DATA (0xCFC)
#define CONFIG_ADDRESS (0xCF8)

uint16_t pci_getHeaderType(uint8_t bus, uint8_t device, uint8_t function){
  return 0;
}


u32 pci_readConfig(uint8_t bus, uint8_t dev, uint8_t func, uint8_t reg){
  u32 add = 0;
  u32 _enable = 1 << 31;
  u32 _bus =  bus << 16;
  u32 _dev =  dev << 11;
  u32 _func = func << 8;
  u32 _reg = reg & 0xFC; // ensure the last two bits are false.

  add = _enable | _bus | _dev | _func | _reg;

  // pump our pci address out to the pci configuration address.
  outl(CONFIG_ADDRESS,add);
  
  // pump the response out of memory.
  u32 data = inl(CONFIG_DATA);

  return data;
}

u16 pci_getVendorID(u8 bus, u8 dev, u8 func){
  u32 data =  pci_readConfig(bus,dev,func,0);
  u16 vendor = (u16)data;

  return vendor;
}

void pci_checkFunction(u8 bus, u8 dev, u8 func){
  u32 data = pci_readConfig(bus,dev,func,0);


  terminal_printf("B: %x D: %x F: %x\n", bus, dev, func);
}

void pci_checkDevice(u8 bus, u8 dev){
  u8 func = 0;
  uint16_t vendorID = pci_getVendorID(bus, dev, func);

  if(vendorID == 0xFFFF){
    return;
  }



  // The device exists and has at least one function.
  // See if what the function is and if there are more functions.
  // why do we check the function? what are we checking it for?
  pci_checkFunction(bus,dev,func); 


  uint16_t headerType = pci_getHeaderType(bus,dev,func);
  if((headerType & 0x80)!=0){
    /* Multi-function device */
    for (func = 1; func <8; func++){
      if (pci_getVendorID(bus,dev,func) != 0xffff){
	pci_checkFunction(bus,dev,func);
      }
    }
  }
}

void pci_scan_bruteforce(){
  uint16_t bus;
  uint8_t dev;
  for (bus=0; bus<256; bus++){
    for(dev=0; dev<32; dev++){
      pci_checkDevice(bus,dev);
    }
  }
}

void pci_scan_test(){
  pci_checkDevice(0,0);
}

void pci_scan(){
  //  pci_scan_test();
  pci_scan_bruteforce();
  terminal_print("PCI scan complete.\n");
}
