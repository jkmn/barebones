C_SOURCES=$(wildcard *.c)
C_OBJECTS=$(C_SOURCES:.c=.o)
AS_SOURCES=$(wildcard *.s)
AS_OBJECTS=$(AS_SOURCES:.s=.o)
OBJECTS=$(C_OBJECTS) $(AS_OBJECTS)
WARNINGS := -Wall -Wextra
CFLAGS := -std=c99 -ffreestanding -O2 -masm=intel $(WARNINGS)

.PHONY: all clean dist check testdrivers todolist

all: kernel.bin

kernel.bin: $(C_OBJECTS) $(AS_OBJECTS)
	echo $(OBJECTS)
	i686-elf-gcc -T linker.ld -o $@ -nostdlib $(CFLAGS) $(OBJECTS)

boot.o: boot.s
	i686-elf-as $< -o $@

isr_wrapper.o: isr_wrapper.s
	i686-elf-as $< -o $@

%.o: %.c
	i686-elf-gcc -c $< -o $@ $(CFLAGS)

clean:
	$(RM) *.o *.bin *~

run: kernel.bin
	qemu-system-i386 -kernel kernel.bin -vnc localhost:5500,reverse
