# Much of this code was shamelessly borrowed from the following address(es):
# http://wiki.osdev.org/Interrupt_Service_Routines

.section .text
.globl	isr_wrapper
.type	isr_wrapper, @function
.align	4

isr_wrapper:
	pusha 
	call isr_default
	popa
	iret
	
