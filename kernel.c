#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#include <stddef.h>
#include <stdint.h>
#include "types.h"
#include "terminal.h"
#include "pci.h"
#include "strings.h"
#include "cpuid.h"
#include "apic.h"
#include "pic.h"
 
/* Check if the compiler thinks if we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif
 
/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif
 

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif
void kernel_main()
{
  terminal_initialize();
  pci_scan();
  cpuid_vendor();
  terminal_printf("Has APIC: %t\n",cpuid_hasAPIC());
  terminal_printf("Has MSR: %t\n",cpuid_hasMSR());
  terminal_printf("APIC Enabled: %t\n", apic_enabled());
  apic_disable();
  terminal_printf("APIC Enabled: %t\n", apic_enabled());
  pic_init();
  terminal_printf("PIC initialized.\n");
  ivt_init();
  terminal_printf("IVT initialized.\n");

}
