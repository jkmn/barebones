#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#include "types.h"

typedef struct
{
  u32 Len;
  u32 Cap;
  char Data[];
} string;



u32 cstr_len(const char* str)
{
  u32 len = 0;
  while ( str[len] != 0 )
    len++;
  return len;
}

/*
void cstr_reverse(char* text){

}

// prints the decimal value of a 32-bit word.
// until we have a heap, this requires a string passed in.
void cstr_dec_from_u32(char* text, const u16 word){

  for (u8 i=0; i<4; i++){
    text[5-i] = "0123456789ABCDEF"[word % 16];
    word = word >> 4;
  }

  //  return cstr;
}
*/

// prints the hex value of a 16-bit word.
// until we have a heap, this requires a string passed in.
void cstr_hex_from_u16(char* text, u16 word){

  //  char cstr[] = "0xFFFF"; // the first two chars are constant.

  for (u8 i=0; i<4; i++){
    text[5-i] = "0123456789ABCDEF"[word % 16];
    word = word >> 4;
  }

  //  return cstr;
}

// prints the hex value of a 32-bit word.
// until we have a heap, this requires a string passed in.
void cstr_hex_from_u32(char* text, u32 word){

  //  char cstr[] = "0xFFFFFFFF"; // the first two chars are constant.

  for (u8 i=0; i<8; i++){
    text[9-i] = "0123456789ABCDEF"[word % 16];
    word = word >> 4;
  }

  //  return cstr;
}

void cstr_append_char(char* dest, const char c){
  u32 len = cstr_len(dest);
  dest[len]=c;
  dest[len+1]='\0';
}

// this could use some optimization. not worried about it now.
void cstr_append_cstr(char* dest, const char* src){
  u32 len = cstr_len(src);
  for (u32 i=0; i<len; i++){
    cstr_append_char(dest, src[i]);
  }
}

void cstr_vsprintf(char* dest, const char* format, va_list args){
  dest[0]='\0'; // effectively set length to 0.

  bool mark = false;
  u32 flen = cstr_len(format);

  for (u8 i=0;i<flen;i++){

    if (mark){
      switch(format[i])
	{

	case '%':
	  cstr_append_char(dest,'%');
	  break;

	case 'c':; // character
	  char	c = (char)va_arg(args,unsigned int);
	  cstr_append_char(dest,c);
	  break;

	case 's':; // cstr
	  char *s = va_arg(args, char*);
	  cstr_append_cstr(dest,s);
	  break;

	case 'x':; // unsigned hexadecimal integer
	  u32 num = va_arg(args,unsigned int);
	  if (num <= 65535){
	    char s[]="0xFFFF";
	    cstr_hex_from_u16(s,(u16)num);
	    cstr_append_cstr(dest,s);
	  }else{
	    char s[]="0xFFFFFFFF";
	    cstr_hex_from_u32(s,num);
	    cstr_append_cstr(dest,s);
	  }
	  break;

	case 't':; // boolean
	  bool test = va_arg(args,unsigned int) > 0;
	  if (test){
	    cstr_append_cstr(dest,"true");
	  }else{
	    cstr_append_cstr(dest,"false");
	  }
	}
      mark = false;
    }else{
      switch(format[i]){

      case '%':
	mark = true;
	break;

      default:
	cstr_append_char(dest,format[i]);

      }
    }
  }
}

void cstr_sprintf(char* dest, const char* format, ...){
  va_list args;
  va_start (args, format);
  cstr_vsprintf(dest,format,args);
  va_end(args);
}

/* pending heap.
// create and initialize a new string.
string str_new(char* c){

u32 len =cstr_len(c);

string s = {len, len+2, (char[len+2])} ;
//  s.Len = len;
//  s.Cap = len+2; // zero value corner case.
//  s.Data[len+2];

for (u32 i=0;i<len;i++){
s.Data[i]=c[i];
}

return s;
}

string str_expand(string s, u32 cap){
u32 c_ = s.Cap;
while (c_ < cap){
c_ = c_ * 1.5;
}

// create an entirely new string.
string s_;
s_.Cap = c_;
s_.Len = s.Len; // Len stays the same.

char _data[c_]; 
s_.Data = _data;

for (u32 i =0; i<s.Len; i++){
s_.Data[i] = s.Data[i];
}
s = s_;
return s;
}

string str_append_char(string s, char c){

s = str_expand(s,s.Len+1);

s.Data[s.Len]=c;
s.Len++;

return s;
}

string str_from_u32(u32 value){

string s = str_new("");

do{
s = str_append_char(s,"0123456789"[value%10]);
}while (value /= 10);

return s;
}

string str_append_str(string s, string t){
s = str_expand(s,(s.Len+t.Len));

for (u32 i = s.Len;i<(s.Len+t.Len);i++){
s.Data[i]=t.Data[i-s.Len];
}

s.Len += t.Len;

return s;
}
*/
