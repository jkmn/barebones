/*
This method is called from the assembly ISR wrapper (isr_wrapper.s).
This mechanism was shamelessly borrowed from the following URL(s):
  http://wiki.osdev.org/Interrupt_Service_Routines
*/

#include "pic.h"
#include "terminal.h"

void isr_default(void){
  terminal_print("I've been interrupted!");
  //  pic_end_eod()
}
