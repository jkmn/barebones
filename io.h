void outb(unsigned short port, unsigned char data);
void outs(unsigned short port, unsigned short data);
void outl(unsigned short port, unsigned long data);
unsigned char inb(unsigned short port);
unsigned short ins(unsigned short port);
unsigned long inl(unsigned short port);
void io_wait();
