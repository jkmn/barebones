#include <stdbool.h>
#include <stdint.h>
#include "types.h"
#include "msr.h"

#define IA32_APIC_BASE_MSR 0x1B
#define IA32_APIC_BASE_MSR_BSP 0x100 // Processor is a BSP
#define IA32_APIC_BASE_MSR_ENABLE 0x800


void apic_enable(){
  u32 lo = 0;
  u32 hi = 0;

  msr_get(IA32_APIC_BASE_MSR, &lo, &hi);

  // mask the enable bit.
  lo = lo | IA32_APIC_BASE_MSR_ENABLE;

  msr_set(IA32_APIC_BASE_MSR, lo, hi);
}


void apic_disable(){
  u32 lo = 0;
  u32 hi = 0;

  msr_get(IA32_APIC_BASE_MSR, &lo, &hi);

  // mask the enable bit.
  lo = lo & ~IA32_APIC_BASE_MSR_ENABLE;

  msr_set(IA32_APIC_BASE_MSR, lo, hi);
}

bool apic_enabled(){
  u32 lo = 0;
  u32 hi = 0;

  msr_get(IA32_APIC_BASE_MSR, &lo, &hi);

  return lo & IA32_APIC_BASE_MSR_ENABLE;
}
