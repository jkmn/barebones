#include <stdint.h>

void msr_get(uint32_t msr, uint32_t *lo, uint32_t *hi)
{
   __asm__ volatile("rdmsr" : "=a"(*lo), "=d"(*hi) : "c"(msr));
}
 
void msr_set(uint32_t msr, uint32_t lo, uint32_t hi)
{
   __asm__ volatile("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));
}
